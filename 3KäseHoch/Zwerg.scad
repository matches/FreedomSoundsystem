module NeutrikNAC3MPXTOP()
{
    color("black")
    {
        translate([0,0,-3.5]) cube([26,31,7], true);
        cylinder(32, 12, 12);
    }
}

module NeutrikFD3NCXTOP()
{
    color("black")
    {
        translate([0,0,-4]) cube([31,38, 8], true);
        cylinder(24, 12, 12);
    }
}

module NeutrikNLT4MP()
{
    color("black")
    {
        translate([0,0,-4]) cube([38,38, 8], true);
        cylinder(29, 16, 16);
    }
}

module RackHousing2UBackplate()
{
    color("grey")
    {
        square([406,86]);
    }
    color("red")
    {
        translate([0,0,1]) square([406,7]);
        translate([0,79,1]) square([406,7]);
    }
    
    translate([0,25])
    {
        translate([30,0]) NeutrikNLT4MP();
        translate([80,0]) NeutrikNLT4MP();
        translate([150,0]) NeutrikNLT4MP();
        translate([200,0]) NeutrikNLT4MP();
        translate([270,0]) NeutrikNLT4MP();
        translate([320,0]) NeutrikNLT4MP();
        
        translate([380,0]) NeutrikNAC3MPXTOP();
    }
    
    translate([0,65])
    {
        translate([30,0]) rotate([0,0,90]) NeutrikFD3NCXTOP();
        translate([80,0]) rotate([0,0,90]) NeutrikFD3NCXTOP();
        translate([150,0]) rotate([0,0,90]) NeutrikFD3NCXTOP();
        translate([200,0]) rotate([0,0,90]) NeutrikFD3NCXTOP();
        translate([270,0]) rotate([0,0,90]) NeutrikFD3NCXTOP();
        translate([320,0]) rotate([0,0,90]) NeutrikFD3NCXTOP();
    }   
}

module RackHousing2UFrontplate()
{
    color("grey") square([483,89], 0);
}

module ICEPower125ASX2()
{
    translate([0,0,10]) color("green") cube([80,160,33]);
    
    translate([5,0])
    {
            translate([0,5]) cylinder(10,2,2);
            translate([0,115]) cylinder(10,2,2);
            translate([0,155]) cylinder(10,2,2);
    }
        
    translate([75,0])
    {
            translate([0,5]) cylinder(10,2,2);
            translate([0,85]) cylinder(10,2,2);
            translate([0,155]) cylinder(10,2,2);
    }
}

module RackHousing2U()
{
    color("grey") square([406,252], 0);
    translate([0,252,0]) rotate([90,0,0]) RackHousing2UBackplate();
    translate([-38.5,0,0]) rotate([90,0,0]) RackHousing2UFrontplate();
}

RackHousing2U();

translate([0,40])
{
    translate([30,0]) ICEPower125ASX2();
    translate([150,0]) ICEPower125ASX2();
    translate([270,0]) ICEPower125ASX2();
}