$woodStrength = 12;

module HornSide(length, width)
{
    translate([-width/2,0,0]) cube([width,length,$woodStrength]);

}

module WavecorTW022WA09()
{
}

module Scanspeak15W4424G00()
{
}

module Horn(cutoffFrequency)
{
    waveLength = 350*1000/cutoffFrequency;
    hornLength = waveLength/4;
    
    side15Length = hornLength/cos(15);
    side30Length = hornLength/cos(30);
    
    mouthY = hornLength*tan(15)*2 +$woodStrength*2;
    mouthX = hornLength*tan(30)*2 +$woodStrength*2;
    
    echo("MouthArea: ", (mouthY/1000)*(mouthX/1000))
    
    color("SaddleBrown") translate([mouthX/2 ,0,mouthY/2]) rotate([0,0,180]){
        rotate([-15,0,0]) translate([0,0,-$woodStrength]) HornSide(side15Length, mouthX);
        rotate([15,0,0]) HornSide(side15Length, mouthX);
        
        rotate([0,90,0]){
            rotate([-30,0,0]) translate([0,0,-$woodStrength]) HornSide(side30Length, mouthY);
            rotate([30,0,0]) HornSide(side30Length, mouthY);
        }
    }
}

module Enclosure(width, length, height){
    color("SaddleBrown"){
        cube([width,length,$woodStrength]);
        translate([0,0,height-$woodStrength]) cube([width,length,$woodStrength]);
        
        translate([$woodStrength,0,$woodStrength]){
            rotate([0,-90,0]) cube([height-$woodStrength*2,length,$woodStrength]);
            translate([width-$woodStrength,0,0]) rotate([0,-90,0]) cube([height-$woodStrength*2,length,$woodStrength]);
        }
    }
}

Enclosure(400, 400, 200);
Horn(cutoffFrequency=300);